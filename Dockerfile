FROM ubuntu

RUN apt-get update -y && \
    apt-get install -y python3-dev \
    python3 \
    ca-certificates \
    wget \
    git \
    && rm -rf /var/lib/apt/lists/*

RUN wget --no-check-certificate https://bootstrap.pypa.io/get-pip.py && \
    python3 get-pip.py

RUN pip3 install awscli --force-reinstall --upgrade

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip3 install -r requirements.txt

COPY . /app

ENTRYPOINT ["python3"]

CMD ["app.py"]